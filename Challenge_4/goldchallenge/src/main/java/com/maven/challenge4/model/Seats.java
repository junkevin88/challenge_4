package com.maven.challenge4.model;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "seats")
public class Seats  implements Serializable {
    @Id
    @Column(name = "seat_number")
    private Long seat_number;

    @Id
    @Column(name = "studio_name")
    private String studio_name;

}
