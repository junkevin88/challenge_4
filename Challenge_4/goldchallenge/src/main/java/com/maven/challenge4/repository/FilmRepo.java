package com.maven.challenge4.repository;

import com.maven.challenge4.model.Films;
import com.maven.challenge4.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import com.fasterxml.jackson.annotation.*;


import javax.persistence.NamedStoredProcedureQueries;

public interface FilmRepo extends PagingAndSortingRepository<Films, Integer> {
    @Query("select c from Films c")// nama class
    public Page<Films> getAllData(Pageable pageable);
}

