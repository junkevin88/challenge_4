package com.maven.challenge4.repository;

import com.maven.challenge4.model.Users;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

public interface UserRepo extends PagingAndSortingRepository<Users, Integer> {
    @Query("select c from Users c")// nama class
    public Page<Users> getAllData(Pageable pageable);
}
