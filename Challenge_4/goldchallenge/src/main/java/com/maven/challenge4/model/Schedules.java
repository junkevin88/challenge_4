package com.maven.challenge4.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "schedules")
public class Schedules extends AbstractDate implements Serializable {

    @Id
    @Column(name = "schedule_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long schedule_id;

    @Column(name = "date_scheduled", nullable = false)
    private Date date_scheduled;

    @Column(name = "ticket _price", nullable = false, length = 30)
    private Float ticket_price;

    @ManyToOne(targetEntity = Films.class, cascade = CascadeType.ALL)
    private Films films;

}

