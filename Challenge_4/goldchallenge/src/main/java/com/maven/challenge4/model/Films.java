package com.maven.challenge4.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "films")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer","handler"})
public class Films implements Serializable {
    @Id
    @Column(name = "film_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long film_id;

    @Column(name = "film_name", nullable = false, length = 100)
    private String film_name;

    @Column(name = "status", nullable = false, length = 2)
    private Boolean status;

    @JsonIgnore
    @OneToMany(targetEntity=Schedules.class)
    private List<Schedules> schedules;
}
