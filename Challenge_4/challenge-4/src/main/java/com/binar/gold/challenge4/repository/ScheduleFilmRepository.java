package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.ScheduleFilm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleFilmRepository extends JpaRepository<ScheduleFilm, Long> {

}
