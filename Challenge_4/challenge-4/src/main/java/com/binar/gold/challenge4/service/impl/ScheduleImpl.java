package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.repository.ScheduleRepository;
import com.binar.gold.challenge4.service.ScheduleService;
import com.binar.gold.challenge4.utils.Config;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ScheduleImpl implements ScheduleService {

    private static Logger logger = LoggerFactory.getLogger(ScheduleImpl.class);
    @Autowired
    public ScheduleRepository scheduleRepository;

    @Autowired
    public Response response;

    @Override
    public Map save(Schedules request) {
        try {
            if (request.getDateScheduled() == null) {
                return response.error("Date scheduled is required!", Config.ERROR_401);
            }
            if (request.getDateStarted() == null) {
                return response.error("Date started is required!", Config.ERROR_401);
            }
            if (request.getDateEnded() == null) {
                return response.error("Date ended is required!", Config.ERROR_401);
            }
            Schedules doSave = scheduleRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public void deleteOne(Long id) {
        scheduleRepository.deleteById(id);
    }
}
