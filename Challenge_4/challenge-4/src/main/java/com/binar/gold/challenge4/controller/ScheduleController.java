package com.binar.gold.challenge4.controller;


import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.repository.ScheduleRepository;
import com.binar.gold.challenge4.service.ScheduleService;
import com.binar.gold.challenge4.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/schedule")
public class ScheduleController {

    @Autowired
    public Response response;

    @Autowired
    public ScheduleRepository scheduleRepository;

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Schedules schedules) {
        return new ResponseEntity<Map>(scheduleService.save(schedules), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteOne(@PathVariable("id") Long id){
        scheduleService.deleteOne(id);
    }
}
