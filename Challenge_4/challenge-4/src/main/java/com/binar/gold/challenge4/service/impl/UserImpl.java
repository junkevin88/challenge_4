package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.entity.Users;
import com.binar.gold.challenge4.repository.UserRepository;
import com.binar.gold.challenge4.service.UserService;
import com.binar.gold.challenge4.utils.Config;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;


@Service
@Transactional
public class UserImpl implements UserService {
    private static Logger logger = LoggerFactory.getLogger(UserImpl.class);
    @Autowired
    public UserRepository userRepository;

    @Autowired
    public Response response;

    @Override
    public Map save(Users request) {
        try {
            if (request.getUsername() == null) {
                return response.error("Username is required!", Config.ERROR_401);
            }
            if (request.getPassword() == null) {
                return response.error("Password is required!", Config.ERROR_401);
            }
            if (request.getEmail() == null) {
                return response.error("Email is required!", Config.ERROR_401);
            }
            Users doSave = userRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }

    @Override
    public Map update(Users request) {
        try {
            if (request.getId() == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Users checkingData = userRepository.getById(request.getId());
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }

            checkingData.setEmail(request.getEmail());
            checkingData.setPassword(request.getPassword());
            checkingData.setUsername(request.getUsername());

            Users doSave = userRepository.save(checkingData);
            return response.sukses(doSave);

        } catch (Exception e) {
            logger.error("Error update, {} " + e);
            return response.error("Error update: " + e.getMessage(), Config.ERROR_500);
        }
    }


    @Override
    public Iterable<Users> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Map delete(Long id) {
        try {
            if (id == null) {
                return response.error("Id is required!", Config.ERROR_401);
            }
            Users checkingData = userRepository.getById(id);
            if (checkingData == null) {
                return response.error("Data cannot be found!", Config.ERROR_404);
            }
            userRepository.deleteById(id);
            return response.sukses(checkingData);
        } catch (Exception e) {
            logger.error("Error delete, {} " + e);
            return response.error("Error delete: " + e.getMessage(), Config.ERROR_500);
        }
    }

}

