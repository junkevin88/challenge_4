package com.binar.gold.challenge4.service;

import com.binar.gold.challenge4.entity.ScheduleFilm;
import com.binar.gold.challenge4.entity.Schedules;

import java.util.Map;

public interface ScheduleFilmService {

    public Iterable<ScheduleFilm> findAll();

    public Map save(ScheduleFilm request);

}
