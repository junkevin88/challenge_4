package com.binar.gold.challenge4.controller;


import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.service.FilmService;
import com.binar.gold.challenge4.utils.Response;
//import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.validation.Errors;
//import org.springframework.validation.ObjectError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

//import javax.validation.Valid;

@RestController
@RequestMapping("/api/film")
public class FilmController {
    @Autowired
    public Response response;

    @Autowired
    public FilmRepository filmRepository;
    @Autowired
    private FilmService filmService;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Films films) {
//        if(errors.hasErrors()){
//            for (ObjectError error : errors.getAllErrors()) {
//                System.err.println(error.getDefaultMessage());
//            }
//            throw new RuntimeException("Validation Error");
//        }

        return new ResponseEntity<Map>(filmService.save(films), HttpStatus.OK);
    }

    @GetMapping("/list")
    public Iterable<Films> findAll(){
        return filmRepository.findAll();
    }
    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Films films){
        return new ResponseEntity<Map>(filmService.update(films), HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Map> getId(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.getById(id), HttpStatus.OK);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> deleteById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(filmService.delete(id), HttpStatus.OK);
    }

    @GetMapping("/status")
    public Iterable<Films> getFilmsFilterStatus(){
        return filmRepository.getFilmsFilterStatus();
    }
}
