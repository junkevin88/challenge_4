package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.controller.UserController;
import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<Users, Long> {
    @Query(value = "select u from Users u WHERE u.id = :idUsers", nativeQuery = false)
    public Users getById(@Param("idUsers") Long id);
}
