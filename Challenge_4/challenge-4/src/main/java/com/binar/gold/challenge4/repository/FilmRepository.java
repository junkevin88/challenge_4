package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface FilmRepository extends JpaRepository<Films, Long> {
    @Query(value = "select f from Films f WHERE f.id = :idfilms", nativeQuery = false)
    public Films getById(@Param("idfilms") Long id);

    @Query(value = "select f from Films f WHERE f.status = TRUE", nativeQuery = false)
    public List<Films> getFilmsFilterStatus();

}
