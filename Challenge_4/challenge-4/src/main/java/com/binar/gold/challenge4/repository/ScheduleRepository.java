package com.binar.gold.challenge4.repository;

import com.binar.gold.challenge4.entity.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Schedules, Long> {
}
