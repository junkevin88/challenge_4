package com.binar.gold.challenge4.entity;


import lombok.*;

import javax.persistence.*;
//import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="films")
@Entity
public class Films {
    @Id
    @Column(name = "film_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @NotEmpty(message = "Film name is required!")
    @Column(name = "film_name")
    private String name;


    //    @NotEmpty(message = "Status is required!")
    @Column(name = "status")
    private Boolean status;

}
