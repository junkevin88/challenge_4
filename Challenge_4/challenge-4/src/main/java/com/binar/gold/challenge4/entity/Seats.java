package com.binar.gold.challenge4.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="seats")
@Entity
public class Seats implements Serializable {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users user;

    @ManyToOne
    @JoinColumn(name = "scheduling_film_id")
    private ScheduleFilm scheduleFilm;

    @Id
    @Column(name = "studio_name")
    private String studioName;

    @Id
    @Column(name = "seat_number")
    private String seatNumber;

}
