package com.binar.gold.challenge4.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="schedule_film")
@Entity
public class ScheduleFilm {
    @Column(name = "film_schedule_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;

    @ManyToOne
    @JoinColumn(name = "film_id")
    private Films films;

    @ManyToOne
    @JoinColumn(name = "schedule_id")
    private Schedules schedules;

}
