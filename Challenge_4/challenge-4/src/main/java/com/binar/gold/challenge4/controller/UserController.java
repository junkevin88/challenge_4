package com.binar.gold.challenge4.controller;

import com.binar.gold.challenge4.entity.Films;
import com.binar.gold.challenge4.entity.Users;
import com.binar.gold.challenge4.repository.UserRepository;
import com.binar.gold.challenge4.service.UserService;
import com.binar.gold.challenge4.utils.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    public Response response;

    @Autowired
    public UserRepository userRepository;
    @Autowired
    private UserService userService;

    @PostMapping(value = {"/save", "/save/"})
    public ResponseEntity<Map> save(@RequestBody Users users) {
//        if(errors.hasErrors()){
//            for (ObjectError error : errors.getAllErrors()) {
//                System.err.println(error.getDefaultMessage());
//            }
//            throw new RuntimeException("Validation Error");
//        }

        return new ResponseEntity<Map>(userService.save(users), HttpStatus.OK);
    }


    @GetMapping("/list")
    public Iterable<Users> findAll(){
        return userService.findAll();
    }
    @PutMapping("/update")
    public ResponseEntity<Map> update(@RequestBody Users users){
        return new ResponseEntity<Map>(userService.update(users), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> deleteById(@PathVariable(value = "id") Long id){
        return new ResponseEntity<Map>(userService.delete(id), HttpStatus.OK);
    }
}
