package com.binar.gold.challenge4.service.impl;

import com.binar.gold.challenge4.entity.ScheduleFilm;
import com.binar.gold.challenge4.entity.Schedules;
import com.binar.gold.challenge4.repository.FilmRepository;
import com.binar.gold.challenge4.repository.ScheduleFilmRepository;
import com.binar.gold.challenge4.service.ScheduleFilmService;
import com.binar.gold.challenge4.utils.Config;
import com.binar.gold.challenge4.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ScheduleFilmImpl implements ScheduleFilmService {
    private static Logger logger = LoggerFactory.getLogger(ScheduleFilmImpl.class);


    @Autowired
    public ScheduleFilmRepository scheduleFilmRepository;

    @Autowired
    public Response response;

    @Override
    public Iterable<ScheduleFilm> findAll() {
        return scheduleFilmRepository.findAll();
    }

    @Override
    public Map save(ScheduleFilm request) {
        try {
            if (request.getSchedules() == null) {
                return response.error("Schedule is required!", Config.ERROR_401);
            }
            if (request.getFilms() == null) {
                return response.error("Film is required!", Config.ERROR_401);
            }
            ScheduleFilm doSave = scheduleFilmRepository.save(request);
            return response.sukses(doSave);
        } catch (Exception e) {
            logger.error("Error save, {} " + e);
            return response.error("Error save: " + e.getMessage(), Config.ERROR_500);
        }
    }
}
